import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { changeFilter } from "../../redux/filter/filter-actions";
import { getImagesFilter } from "../../redux/filter/filter-selectors";
import {
  getCounterForAlbum,
  getCounterForTitle,
} from "../../redux/counter/counter-selectors";
import {
  changeCounterForAlbumOperation,
  changeCounterForTitleOperation,
} from "../../redux/counter/counter-operation";
import ButtonWithImg from "../ButtonWithImg/ButtonWithImg";
import { HeaderWrapper } from "./HeaderStyled";

export default function Header() {
  const dispatch = useDispatch();
  const filter = useSelector(getImagesFilter);
  const counterForAlbum = useSelector(getCounterForAlbum);
  const counterForTitle = useSelector(getCounterForTitle);
  const currentPath = window.location.pathname;
  const [buttonSortStyleForAlbum, setButtonSortStyleForAlbum] =
    useState("green");
  const [buttonSortStyleForTitle, setButtonSortStyleForTitle] =
    useState("green");
  const handleImageNameChange = (event) => {
    return dispatch(changeFilter(event.target.value));
  };

  const onChangeCount = (e) => {
    e.preventDefault();
    console.log(e.target);

    if (e.target.nodeName !== "SPAN") return;
    if (e.target.outerText === "sort") {
      console.log("counterForAlbum", counterForAlbum);
      switch (counterForAlbum) {
        case 3:
          setButtonSortStyleForAlbum("green-down");
          dispatch(changeCounterForAlbumOperation(1));
          break;
        case 2:
          setButtonSortStyleForAlbum("green");
          dispatch(changeCounterForAlbumOperation(3));
          break;
        case 1:
          setButtonSortStyleForAlbum("green-up");
          dispatch(changeCounterForAlbumOperation(2));
          break;
        default:
          setButtonSortStyleForAlbum("green");
          dispatch(changeCounterForAlbumOperation(3));
          break;
      }
    }
    if (e.target.outerText === "sort_by_alpha") {
      console.log("counterForTitle", counterForTitle);
      switch (counterForTitle) {
        case 3:
          setButtonSortStyleForTitle("green-down");
          dispatch(changeCounterForTitleOperation(1));
          break;
        case 2:
          setButtonSortStyleForTitle("green");
          dispatch(changeCounterForTitleOperation(3));
          break;
        case 1:
          setButtonSortStyleForTitle("green-up");
          dispatch(changeCounterForTitleOperation(2));
          break;
        default:
          setButtonSortStyleForTitle("green");
          dispatch(changeCounterForTitleOperation(3));
          break;
      }
    }
  };

  return (
    <HeaderWrapper>
      <div className="SearchForm">
        <input
          className="SearchForm-input"
          type="text"
          autoComplete="off"
          autoFocus
          placeholder="Search images and photos"
          name="imageNameSearch"
          onChange={handleImageNameChange}
          value={filter}
        />
        <div className="wrapperForSort">
          <ButtonWithImg
            onHandleClick={onChangeCount}
            icon={"sort"}
            classBtn={"sortButton"}
            background={buttonSortStyleForAlbum}
            name="sort-by-number"
          />
          <ButtonWithImg
            onHandleClick={onChangeCount}
            icon={"sort_by_alpha"}
            classBtn={"sortButton"}
            background={buttonSortStyleForTitle}
            name="sort-by-alpha"
          />
        </div>
      </div>
      <NavLink
        exact="true"
        to={`${currentPath === "/favorites" ? "/" : "/favorites"}`}
        className={`navLink-item ${
          currentPath === "/favorites" ? "purpleColor" : "redColor"
        }`}
      >
        {`${currentPath === "/favorites" ? "back to home" : "favorites"}`}
      </NavLink>
    </HeaderWrapper>
  );
}
