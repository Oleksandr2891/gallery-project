import {
  addFavoritesSuccess,
  deleteFavoritesSuccess,
} from "./favorites-actions";

export const addFavoritesOperation = (id) => async (dispatch) => {
  dispatch(addFavoritesSuccess(id));
};

export const deleteFavoritesOperation = (id) => async (dispatch) => {
  dispatch(deleteFavoritesSuccess(id));
};
