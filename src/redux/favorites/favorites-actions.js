import { createAction } from "@reduxjs/toolkit";

const addFavoritesSuccess = createAction("favorites/addFavoritesSuccess");
const addFavoritesError = createAction("favorites/addFavoritesError");

const deleteFavoritesSuccess = createAction("favorites/deleteFavoritesSuccess");
const deleteFavoritesError = createAction("favorites/deleteFavoritesError");
export {
  addFavoritesSuccess,
  addFavoritesError,
  deleteFavoritesSuccess,
  deleteFavoritesError,
};
