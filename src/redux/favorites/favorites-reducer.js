import { createReducer } from "@reduxjs/toolkit";
import {
  addFavoritesSuccess,
  deleteFavoritesSuccess,
} from "./favorites-actions";
const initState = { id: [] };

const favoritesReducer = createReducer(initState, {
  [addFavoritesSuccess]: (state, { payload }) => {
    const result = [payload];
    return result;
  },
  [deleteFavoritesSuccess]: (state, { payload }) => {
    return state.filter((item) => item !== payload);
  },
});

export default favoritesReducer;
