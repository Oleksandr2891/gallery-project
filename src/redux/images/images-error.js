import { createReducer } from "@reduxjs/toolkit";
import { getAsyncImagesError, getAsyncImagesRequests } from "./images-actions";

const error = createReducer(null, {
  [getAsyncImagesRequests]: () => null,
  [getAsyncImagesError]: (_, { payload }) => payload.message,
});

export default error;
