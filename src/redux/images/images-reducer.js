import { createReducer } from "@reduxjs/toolkit";
import { getAsyncImagesSuccess } from "./images-actions";

const image = createReducer([], {
  [getAsyncImagesSuccess]: (_, { payload }) => payload,
});

export default image;
