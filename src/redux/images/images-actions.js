import { createAction } from "@reduxjs/toolkit";

export const changeFilter = createAction("contacts/changeFilter");

export const getAsyncImagesRequests = createAction("images/getImagesRequests");
export const getAsyncImagesSuccess = createAction("images/getImagesSuccess");
export const getAsyncImagesError = createAction("Images/getImagesError");
