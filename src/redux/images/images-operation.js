import getImages from "../../entities/Api";
import {
  getAsyncImagesError,
  getAsyncImagesRequests,
  getAsyncImagesSuccess,
} from "./images-actions";

export const getGallery = () => async (dispatch) => {
  dispatch(getAsyncImagesRequests());
  try {
    const images = await getImages();
    dispatch(getAsyncImagesSuccess(images));
  } catch (error) {
    dispatch(getAsyncImagesError(error));
  }
};
