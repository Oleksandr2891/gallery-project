import { createSelector } from "@reduxjs/toolkit";
import { visibleImage } from "../../entities/helpers-function";
import { getGallerySelector } from "../images/images-selectors";

export const getImagesFilter = (state) => state.images.filter;

export const getVisibleImages = createSelector(
  [getGallerySelector, getImagesFilter],
  (items, filter) => visibleImage(items, filter)
);
