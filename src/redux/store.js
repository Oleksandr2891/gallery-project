import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import thunk from "redux-thunk";
import logger from "redux-logger";
import image from "./images/images-reducer";
import filter from "./filter/filter-reducer";
import error from "./images/images-error";
import favoritesReducer from "./favorites/favorites-reducer";
import storage from "redux-persist/lib/storage";
import { counterForTitle, counterForAlbum } from "./counter/counter-reducer";

const favoritesConfig = {
  key: "favorites",
  storage,
};

const images = combineReducers({
  image,
  filter,
  error,
});

const counters = combineReducers({
  counterForTitle,
  counterForAlbum,
});

export const store = configureStore({
  reducer: {
    images,
    counters,
    favorites: persistReducer(favoritesConfig, favoritesReducer),
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(logger, thunk),
  devTools: process.env.NODE_ENV === "development",
});

export const persistor = persistStore(store);
