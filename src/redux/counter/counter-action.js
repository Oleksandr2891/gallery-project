import { createAction } from "@reduxjs/toolkit";

export const changeCounterForTitle = createAction(
  "counterForTitle/changeCounterForTitle"
);

export const changeCounterForAlbum = createAction(
  "counterForAlbum/changeCounterForAlbum"
);
