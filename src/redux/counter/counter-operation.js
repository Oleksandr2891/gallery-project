import { changeCounterForTitle, changeCounterForAlbum } from "./counter-action";

export const changeCounterForTitleOperation = (id) => async (dispatch) => {
  dispatch(changeCounterForTitle(id));
};

export const changeCounterForAlbumOperation = (id) => async (dispatch) => {
  dispatch(changeCounterForAlbum(id));
};
