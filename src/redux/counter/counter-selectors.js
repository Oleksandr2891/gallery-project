export const getCounterForTitle = (state) => state.counters.counterForTitle;
export const getCounterForAlbum = (state) => state.counters.counterForAlbum;
