import { createReducer } from "@reduxjs/toolkit";
import { changeCounterForTitle, changeCounterForAlbum } from "./counter-action";

export const counterForTitle = createReducer(3, {
  [changeCounterForTitle]: (_, { payload }) => payload,
});

export const counterForAlbum = createReducer(3, {
  [changeCounterForAlbum]: (_, { payload }) => payload,
});
