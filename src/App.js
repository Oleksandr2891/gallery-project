import { Route, Routes } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Suspense, useEffect } from "react";
import { getGallery } from "./redux/images/images-operation";
import Home from "./pages/home/home";
import LoaderSpiner from "./components/loader/Loader";
import Favorites from "./pages/favorites/favorites";
import GlobalStyles from "./GlobalStyle";

function App() {
  const dispatch = useDispatch();

  const getListImageGallery = async () => {
    try {
      await dispatch(getGallery());
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    getListImageGallery();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <Suspense fallback={<LoaderSpiner />}>
        <GlobalStyles />
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
        <Routes>
          <Route path="/favorites" element={<Favorites />} />
        </Routes>
      </Suspense>
    </>
  );
}

export default App;
