import axios from "axios";

const configUrl = {
  url: "https://jsonplaceholder.typicode.com/photos",
};

const getImages = () =>
  axios
    .get(`${configUrl.url}`)
    .then((response) => response.data)
    .catch((err) => alert(err));

export default getImages;
