let isNotFoundName = false;

export const visibleImage = (item, filter) => {
  const normalizedFilter = filter?.toLowerCase();
  const images = item?.filter((image) =>
    image.title.toLowerCase().includes(normalizedFilter)
  );
  if (filter && !images.length && !isNotFoundName) {
    isNotFoundName = true;
  }
  if (images.length) isNotFoundName = false;
  if (filter) {
    return images;
  } else {
    return item;
  }
};

export const sortByTitleDown = (arr) => {
  function SortArray(x, y) {
    return x.title.localeCompare(y.title);
  }
  const sort = arr.sort(SortArray);
  return sort;
};

export const sortByTitleUp = (arr) => {
  function SortArray(x, y) {
    return y.title.localeCompare(x.title);
  }
  const sort = arr.sort(SortArray);
  return sort;
};

export const sortByAlbumOfNumberDown = (arr) => {
  const sort = arr.sort(function (a, b) {
    return a.albumId - b.albumId;
  });
  return sort;
};

export const sortByAlbumOfNumberUp = (arr) => {
  const sort = arr.sort(function (a, b) {
    return b.albumId - a.albumId;
  });
  return sort;
};
