import ButtonWithImg from "../ButtonWithImg/ButtonWithImg";
import { GalleryItem } from "./GalleryItemStyled";

const ImageGalleryItem = ({
  isFavoriteItem = false,
  albumId,
  id,
  title,
  webformatURL,
  largeImageURL,
  openModalWithImage,
  addFavorites,
}) => {
  const openImage = () => openModalWithImage(largeImageURL, title, albumId, id);

  const onHandleClick = () => addFavorites(id);

  return (
    <GalleryItem>
      <img
        src={webformatURL}
        alt={title}
        className="ImageGalleryItem-image"
        onClick={openImage}
      />
      <div className="wrapperBottom">
        <div className="wrapperText">
          <p>Number album: {albumId}</p>
          <p>Title: {title}</p>
        </div>
        <ButtonWithImg
          icon={!isFavoriteItem ? "favorite_border" : "favorite"}
          background={"red"}
          classBtn={"favorite"}
          onHandleClick={onHandleClick}
        />
      </div>
    </GalleryItem>
  );
};

export default ImageGalleryItem;
