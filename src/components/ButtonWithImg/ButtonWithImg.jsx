import { ButtonWrapper } from "./ButtonWithImgStyled";
import "material-icons/iconfont/material-icons.css";

const ButtonWithImg = ({
  onHandleClick,
  icon = "add",
  classBtn = "add",
  background = "",
}) => {
  return (
    <ButtonWrapper>
      <button
        type="button"
        className={`${classBtn} btnCommon`}
        onClick={onHandleClick}
      >
        <span className={`material-icons icon ${background}`}>{icon}</span>
      </button>
    </ButtonWrapper>
  );
};
export default ButtonWithImg;
