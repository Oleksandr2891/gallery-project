import styled from "styled-components";

export const ButtonWrapper = styled.div`
  .btnCommon {
    width: 40px;
    height: 40px;
    border-radius: 5px;
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    transition: all 500ms cubic-bezier(0.075, 0.82, 0.165, 1);
    &:focus,
    &:hover {
      outline: none;
      box-shadow: 0px 3px 4px rgba(255, 107, 8, 0.3);
      transform: scale(1.15);
    }
  }
  .favorite {
    width: 44px;
    height: 44px;
    font-size: 26px;
  }

  .red {
    color: red;
  }

  .green {
    color: green;
  }

  .green-down {
    background-color: #3822eb;
    color: #ffffff;
  }

  .green-up {
    background-color: #4700ff;
    color: #d50b0b;
  }
  .icon {
    font-size: 34px;
  }
`;
