import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { nanoid } from "nanoid";
import ImageGalleryItem from "../ImageGalleryItem/ImageGalleryItem";
import LoaderSpiner from "../loader/Loader";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import ButtonWithImg from "../ButtonWithImg/ButtonWithImg";
import { ImageGalleryList } from "./ImageGalleryStyled";
import "react-toastify/dist/ReactToastify.css";
import {
  getImagesFilter,
  getVisibleImages,
} from "../../redux/filter/filter-selectors";
import {
  getCounterForAlbum,
  getCounterForTitle,
} from "../../redux/counter/counter-selectors";
import { imagesIsFavorites } from "../../redux/favorites/favorites-selectors";
import { addFavoritesOperation } from "../../redux/favorites/favorites-operations";
import {
  sortByAlbumOfNumberDown,
  sortByAlbumOfNumberUp,
  sortByTitleDown,
  sortByTitleUp,
} from "../../entities/helpers-function";

const initialCrdential = {
  path: "",
  title: "",
  albumId: "",
};
let initialnumberOfViewedImages = 15;

export default function ImageGallery() {
  const dispatch = useDispatch();
  const image = useSelector(getVisibleImages);
  const filter = useSelector(getImagesFilter);
  const favorites = useSelector(imagesIsFavorites);
  const counterForAlbum = useSelector(getCounterForAlbum);
  const counterForTitle = useSelector(getCounterForTitle);
  const [numberOfViewedImages, setNumberOfViewedImages] = useState(
    initialnumberOfViewedImages
  );
  const [imageData, setImageData] = useState([]);
  const [isSort, setIsSort] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [onLoadImage, setOnLoadImage] = useState(false);
  const [imageCredential, setImageCredential] = useState(initialCrdential);
  const currentPath = window.location.pathname;
  let favoriteImages = [];

  const getListOfViewedImages = async (numberOfViewedImages) => {
    setIsModalOpen(true);
    setOnLoadImage(true);
    try {
      if (image.length) {
        let listOfViewedImages;
        if (currentPath === "/favorites") {
          image.forEach((img) => {
            if (favorites[0].includes(img.id)) favoriteImages.push(img);
          });
          if (favoriteImages.length > numberOfViewedImages) {
            listOfViewedImages = favoriteImages.slice(
              imageData.length,
              numberOfViewedImages
            );
          } else {
            listOfViewedImages = favoriteImages;
          }
        } else {
          if (image.length > numberOfViewedImages) {
            listOfViewedImages = image.slice(
              imageData.length,
              numberOfViewedImages
            );
          } else {
            listOfViewedImages = image;
          }
        }
        const imageDataWithKey = listOfViewedImages.map((item) => ({
          ...item,
          key: nanoid(10),
        }));
        setImageData((prevImageData) =>
          prevImageData
            ? [...prevImageData, ...imageDataWithKey]
            : imageDataWithKey
        );
      }
    } catch (error) {
      console.log(error);
    } finally {
      setIsModalOpen(false);
      setOnLoadImage(false);

      numberOfViewedImages !== initialnumberOfViewedImages &&
        window.scrollTo({
          top: document.documentElement.scrollHeight,
          behavior: "smooth",
        });
      console.log("imageDatagetListOfViewedImages", imageData);
    }
  };

  useEffect(() => {
    switch (counterForAlbum) {
      case 1:
        setIsSort(true);
        setImageData(sortByAlbumOfNumberDown(imageData));
        break;
      case 2:
        setIsSort(true);
        setImageData(sortByAlbumOfNumberUp(imageData));
        break;
      case 3:
        setImageData([]);
        break;
      default:
        console.log("Sorry, we are out of function");
    }
  }, [counterForAlbum]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    switch (counterForTitle) {
      case 1:
        setIsSort(true);
        setImageData(sortByTitleDown(imageData));
        break;
      case 2:
        setIsSort(true);
        setImageData(sortByTitleUp(imageData));
        break;
      case 3:
        setImageData([]);
        break;
      default:
        console.log("Sorry, we are out of function");
    }
  }, [counterForTitle]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (currentPath === "/favorites") {
      if (
        imageData?.length < initialnumberOfViewedImages &&
        favorites[0].length !== imageData?.length
      ) {
        getListOfViewedImages(numberOfViewedImages);
      }
    } else {
      if (
        imageData?.length < initialnumberOfViewedImages &&
        image.length > initialnumberOfViewedImages
      ) {
        getListOfViewedImages(numberOfViewedImages);
      }
    }
  }, [imageData, image]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (isSort === true && imageData.length === 0) {
      console.log("second");
      if (
        imageData?.length < initialnumberOfViewedImages &&
        (favorites[0].length !== imageData?.length ||
          image.length > initialnumberOfViewedImages)
      ) {
        getListOfViewedImages(numberOfViewedImages);
        setIsSort(false);
      }
    }
  }, [imageData, image, isSort]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (filter !== "") {
      setImageData([]);
      setNumberOfViewedImages(initialnumberOfViewedImages);
      getListOfViewedImages(numberOfViewedImages);
    }
  }, [filter]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (numberOfViewedImages !== initialnumberOfViewedImages)
      getListOfViewedImages(numberOfViewedImages);
  }, [numberOfViewedImages]); // eslint-disable-line react-hooks/exhaustive-deps

  const closeModal = () => {
    setIsModalOpen(false);
    setImageCredential(initialCrdential);
  };
  const openModalWithImage = (path, title, albumId, id) => {
    setIsModalOpen(true);
    setImageCredential({ path, title, albumId, id });
  };
  const onHandleLoadMore = () => {
    return setNumberOfViewedImages(
      (prevNumberOfViewedImages) =>
        prevNumberOfViewedImages + initialnumberOfViewedImages
    );
  };

  const onHandleClick = (id) => {
    let newFavorites = [];
    if (!favorites[0] || favorites[0]?.length === 0) {
      newFavorites[0] = id;
    } else if (!!favorites[0].find((item) => item === id)) {
      newFavorites = favorites[0].filter((item) => item !== id);
    } else {
      newFavorites = [...favorites[0], id];
    }
    return dispatch(addFavoritesOperation(newFavorites));
  };

  console.log("imageDate", imageData);
  console.log("initialnumberOfViewedImages", initialnumberOfViewedImages);

  return (
    <>
      {!imageData ? (
        <LoaderSpiner />
      ) : (
        <ImageGalleryList>
          {imageData &&
            imageData.map(({ albumId, key, thumbnailUrl, url, title, id }) => {
              return (
                <ImageGalleryItem
                  isFavoriteItem={favorites[0]?.includes(id)}
                  key={key}
                  albumId={albumId}
                  id={id}
                  title={title}
                  webformatURL={thumbnailUrl}
                  largeImageURL={url}
                  openModalWithImage={openModalWithImage}
                  addFavorites={onHandleClick}
                />
              );
            })}
        </ImageGalleryList>
      )}
      {imageData?.length >= initialnumberOfViewedImages && (
        <Button onHandleLoadMore={onHandleLoadMore} />
      )}
      {isModalOpen && (
        <Modal closeModal={closeModal}>
          {onLoadImage ? (
            <LoaderSpiner />
          ) : (
            <div>
              <img
                src={imageCredential.path}
                width="1000"
                height="600"
                alt="ItisPhoto"
                className="imageInModal"
              />
              <div className="bottomBlock">
                <div className="wrapperTitle">
                  <p>Title: {imageCredential.title}</p>
                  <p>Number of album: {imageCredential.albumId}</p>
                </div>
                <div className="wrapperButton">
                  <ButtonWithImg
                    icon={
                      !favorites[0]?.includes(imageCredential.id)
                        ? "favorite_border"
                        : "favorite"
                    }
                    background={"red"}
                    classBtn={"favorite"}
                    onHandleClick={() => onHandleClick(imageCredential.id)}
                  />
                  <ButtonWithImg
                    icon={"close"}
                    background={"green"}
                    classBtn={"favorite"}
                    onHandleClick={closeModal}
                  />
                </div>
              </div>
            </div>
          )}
        </Modal>
      )}
    </>
  );
}
