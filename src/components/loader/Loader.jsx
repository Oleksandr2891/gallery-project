import { BallTriangle } from "react-loader-spinner";
import { LoaderPosition } from "./LoaderStyled";

const LoaderSpiner = () => {
  return (
    <LoaderPosition>
      <BallTriangle
        heigth="300"
        width="300"
        color="#48ff00"
        ariaLabel="loading-indicator"
      />
    </LoaderPosition>
  );
};

export default LoaderSpiner;
