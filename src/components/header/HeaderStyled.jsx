import styled from "styled-components";

export const HeaderWrapper = styled.header`
  top: 0;
  left: 0;
  position: sticky;
  z-index: 1100;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 64px;
  padding-right: 24px;
  padding-left: 24px;
  padding-top: 12px;
  padding-bottom: 12px;
  color: #fff;
  background-color: #3f51b5;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);

  .SearchForm {
    display: flex;
    align-items: center;
    width: 100%;
    max-width: 600px;
    background-color: #fff;
    border-radius: 3px;
    overflow: hidden;
  }
  .wrapperForSort {
    display: flex;
    width: 96px;
    justify-content: space-between;
  }

  .SearchForm-input {
    display: inline-block;
    width: 100%;
    font: inherit;
    font-size: 20px;
    border: none;
    outline: none;
    padding-left: 4px;
    padding-right: 4px;
  }

  .SearchForm-input::placeholder {
    font: inherit;
    font-size: 18px;
  }
  .purpleColor {
    background-color: #c2fbd7;
    box-shadow: rgba(44, 187, 99, 0.2) 0 -25px 18px -14px inset,
      rgba(44, 187, 99, 0.15) 0 1px 2px, rgba(44, 187, 99, 0.15) 0 2px 4px,
      rgba(44, 187, 99, 0.15) 0 4px 8px, rgba(44, 187, 99, 0.15) 0 8px 16px,
      rgba(44, 187, 99, 0.15) 0 16px 32px;
    color: green;
    &:hover {
      box-shadow: rgba(44, 187, 99, 0.35) 0 -25px 18px -14px inset,
        rgba(44, 187, 99, 0.25) 0 1px 2px, rgba(44, 187, 99, 0.25) 0 2px 4px,
        rgba(44, 187, 99, 0.25) 0 4px 8px, rgba(44, 187, 99, 0.25) 0 8px 16px,
        rgba(44, 187, 99, 0.25) 0 16px 32px;
      transform: scale(1.05) rotate(-1deg);
    }
  }

  .redColor {
    background-color: #fbc4c2;
    box-shadow: rgba(187, 32, 32, 0.2) 0 -25px 18px -14px inset,
      rgba(187, 32, 32, 0.15) 0 1px 2px, rgba(187, 32, 32, 0.15) 0 2px 4px,
      rgba(187, 32, 32, 0.15) 0 4px 8px, rgba(187, 32, 32, 0.15) 0 8px 16px,
      rgba(187, 32, 32, 0.15) 0 16px 32px;
    color: red;
    &:hover {
      box-shadow: rgba(187, 32, 32, 0.35) 0 -25px 18px -14px inset,
        rgba(187, 32, 32, 0.25) 0 1px 2px, rgba(187, 32, 32, 0.25) 0 2px 4px,
        rgba(187, 32, 32, 0.25) 0 4px 8px, rgba(187, 32, 32, 0.25) 0 8px 16px,
        rgba(187, 32, 32, 0.25) 0 16px 32px;
      transform: scale(1.05) rotate(-1deg);
    }
  }

  .navLink-item {
    margin-left: 20px;
    text-decoration: none;
    display: inline-block;
    padding: 7px 20px;
    text-align: center;
    font-family: CerebriSans-Regular, -apple-system, system-ui, Roboto,
      sans-serif;
    border: 0;
    border-radius: 100px;
    font-size: 16px;

    transition: all 250ms;
    cursor: pointer;
    user-select: none;
    -webkit-user-select: none;
    touch-action: manipulation;
  }
`;
