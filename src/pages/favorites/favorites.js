import ImageGallery from "../../components/imageGallary/ImageGallery";
import Header from "../../components/header/Header";
import { FavoritesWrapper } from "./favoritesStyled";

const Favorites = () => {
  return (
    <FavoritesWrapper>
      <Header />
      <ImageGallery />
    </FavoritesWrapper>
  );
};

export default Favorites;
