import ImageGallery from "../../components/imageGallary/ImageGallery";
import Header from "../../components/header/Header";
import { HomeWrapper } from "./homeStyled";

const Home = () => {
  return (
    <HomeWrapper>
      <Header />
      <ImageGallery />
    </HomeWrapper>
  );
};

export default Home;
